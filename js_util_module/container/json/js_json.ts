/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
interface ArkPrivate {
    Json: number;
    Load(key: number): Object;
  }
  let flag: boolean = false;
  let fastJson: Object = undefined;
  let arkPritvate: ArkPrivate = globalThis.ArkPrivate || undefined;
  if (arkPritvate !== undefined) {
    fastJson = arkPritvate.Load(arkPritvate.Json);
  } else {
    flag = true;
  }
  declare function requireNapi(s: string): any;
  if (flag || fastJson === undefined) {
    const { errorUtil } = requireNapi('util.struct');
    class HandlerJson<T> {
    }
    interface IterableIterator<T> {
      next: () => {
        value: T;
        done: boolean;
      };
    }
    class Json<T> {

    }
    Object.freeze(Json);
    fastJson = Json;
  }
  export default fastJson;
  